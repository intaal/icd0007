<?php

// Retrieve configuration
$appConfig = require __DIR__ . '/../config/application.config.php';

//Application initialisation/entry point. 
if (isset($_GET['cmd']))
{
    // router 
    switch ($_GET['cmd'])
    {
        case 'about':
            $controller_name = 'IndexController';
            $action = 'aboutCmd';
            break;
        
        case 'post':
            $controller_name = 'AppController';
            $action = 'postCmd';
            break;
        
        case 'addcontact':
            $controller_name = 'AppController';
            $action = 'addCmd';
            break;

        case 'editcontact':
            $controller_name = 'AppController';
            $action = 'editCmd';
            break;
        
        case 'addcontactsubmitted':
            $controller_name = 'AppController';
            $action = 'addcontactsubmittedCmd';
            break;

        case 'editcontactsubmitted':
            $controller_name = 'AppController';
            $action = 'editcontactsubmittedCmd';
            break;
        
        case 'login':
            $controller_name = 'IndexController';
            $action = 'loginCmd';
            break;
        
        case 'loginsubmitted':
            $controller_name = 'IndexController';
            $action = 'loginsubmittedCmd';
            break;
        
        case 'logout':
            $controller_name = 'IndexController';
            $action = 'logoutCmd';
            break;

        case 'app':
        default:
            $controller_name = 'AppController';
            $action = 'indexCmd';
            break;

        case 'lang':
            $controller_name = 'IndexController';
            $action = 'langCmd';
            break;
    }
} else
{
    $controller_name = 'AppController';
    $action = 'indexCmd';

}
require '../Application/Controller/' . $controller_name . '.php';

require '../Application/Model/AppManager.php';
require '../Application/Model/UserManager.php';
require '../Application/View/Init.php';
$appManager = new AppManager($appConfig);
$userManager = new UserManager($appConfig);
$controller = new $controller_name($appManager, $userManager);
$controller->{$action}($_REQUEST);

