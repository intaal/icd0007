-- To create a new database, run MySQL client:
--   mysql -u root -p <password>
-- Then in MySQL client command line, type the following (replace <password> with password string):
--   create database app;
--   grant all privileges on ContactInfoApp.* to root@localhost identified by '<password>';
--   quit
-- Then, in shell command line, type:
--   mysql -u root -p <password> < schema.mysql.sql

-- CREATE SCHEMA `contactinfoapp` ;

set names 'utf8';

-- Contact
CREATE TABLE `contact` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT, -- Unique ID
  `firstname` text NOT NULL,     -- Eesnimi
  `lastname` text NOT NULL,
  `birthdate` text NOT NULL,
  `contact_url` varchar(128) DEFAULT NULL, -- Contact's URL of homepage
  `contact_email` varchar(128) DEFAULT NULL,
  `contact_description` text NOT NULL,          -- Text
  `status` int(11) NOT NULL,        -- Status  
  `id_user` int(11) NOT NULL,       -- User's ID of Contact
  `date_created` DATETIME DEFAULT   CURRENT_TIMESTAMP -- Creation date
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='utf8_general_ci';

INSERT INTO contact(`id`, `firstname`, `lastname`, `birthdate`,`contact_url`,`contact_email`, `contact_description`, `status`, `date_created`, id_user) VALUES
(
    1,
   'Shuurik',
   'Lame',
   '05/10/2020',
   'https://www.delfi.ee',
   'shuuriklame@gmail.com',
   'Suht võll vend olen, arendan hullult palju, eriti PHPs, sest see eelmise dekaadi pärand ning tööd on palju.',
    2,
   '2017-07-06 10:50',1);

CREATE TABLE `phonenumbers` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT, -- Unique ID
  `phonenumber` int(11) NOT NULL,
  `id_contact` int(11) ,
  `status` int(11) NOT NULL,        -- Status
  `date_created` DATETIME DEFAULT   CURRENT_TIMESTAMP -- Creation date
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='utf8_general_ci';

INSERT INTO `phonenumbers` (`id`, `phonenumber`, `id_contact`, `status`, `date_created`) VALUES
(
    1,
    55656561,
    1,
    2,
   '2017-07-06 10:50'),
   (
    2,
    55656562,
    1,
    2,
   '2017-09-06 10:50'
   ),
    (
    3,
    55656563,
    1,
    2,
   '2018-07-06 10:50'
   );

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `email`    varchar(100) NOT NULL UNIQUE,
  `name`        varchar(250) DEFAULT NULL,
  `password`    varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `user` (`id`, `email`, `name`, `password`) VALUES
(1, 'intaal@ttu.ee', 'Indrek Taal', '29aa6f7328d367f4d23ddd15950fea53'),
(2, 'user', 'testkasutaja', '5ebe2294ecd0e0f08eab7690d2a6ee69');