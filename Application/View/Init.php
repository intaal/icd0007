<?php
class appView
{

    private $converter;
    private $content; // content of page output


    public function __construct(AppManager $converter)
    {
        $this->converter = $converter;


        $tmp = debug_backtrace();
        $this->controller = str_replace("controller", "", strtolower($tmp[1]['class']));
        $this->action = str_replace("cmd", "", strtolower($tmp[1]['function']));
    }

    public function __destruct()
    {
        include '../Application/View/Layout/layout.phtml';
    }

    public function renderView($variables = null)
    {
        // Report all errors except E_NOTICE
        error_reporting(E_ALL & ~E_NOTICE);
        ob_start();

        if($_COOKIE["lang"] == "ee" OR $_COOKIE["lang"] == "en")
        {
            require "../Application/View/".$_COOKIE["lang"]."/$this->controller/$this->action.phtml";
        }
        else
        {
            setcookie("lang", "ee",time() + 60*60*24*30);
            require "../Application/View/ee/$this->controller/$this->action.phtml";
        }
        $this->content = ob_get_clean();
    }


}

