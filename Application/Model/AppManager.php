<?php

class AppManager
{

    /*Needs total refactor for proper OOP style, here most is hacked together quickly*/

    private $connectionParam = [
        'host' => '',
        'port' => '',
        'user' => '',
        'password' => '',
        'dbname' => ''
    ];
    private $db;

    public function __construct($appConfig = null)
    {
        $this->connectionParam = $appConfig['connection']['params'];
        
        
        $this->db = new mysqli($this->connectionParam['host'], $this->connectionParam['user'], $this->connectionParam['password'], $this->connectionParam['dbname']);
        
        
        if (mysqli_connect_errno())
        {
            printf("Connect failed: %s\n", mysqli_connect_error());
            if (strpos(mysqli_connect_error(), "Unknown database") !== NULL)
            {
                $this->install();
            }
            exit();
        }
    }
    
    private function install()
    {
        $conn = new mysqli($this->connectionParam['host'], $this->connectionParam['user'], $this->connectionParam['password']);
        if ($conn->connect_error)
        {
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = "CREATE DATABASE " . $this->connectionParam['dbname'];
        if ($conn->query($sql) === TRUE)
        {
            echo "Database created successfully";
            $sql = file_get_contents('../data/schema.mysql.sql');
            if (mysqli_multi_query($conn, $sql))
            {
                echo "Database installed successfully";
            } else
            {
                echo "Error installing database: " . $conn->error;
            }
        } else
        {
            echo "Error creating database: " . $conn->error;
        }
        $conn->close();
    }
    

    /**
     * Get all contacts from the database
     * @return array
     */
    public function findAllContacts()
    {
        $posts = array();
        $query = ""
                . "SELECT contact.* "
                . "FROM contact "
                . "LEFT JOIN user ON contact.id_user = user.id "
                . "WHERE status > 0 "
                . "ORDER BY contact.date_created DESC";
        $result = $this->db->query($query);
        if ($result)
        {
            // Cycle through results
            while ($row = $result->fetch_assoc()) {
                $posts[] = array(
                    'id' => $row['id'],
                    'firstName' => $row['firstname'],
                    'lastName' => $row['lastname'],
                    'birthdate' => $row['birthdate'],
                    'contact_url' => $row['contact_url'],
                    'contact_email' => $row['contact_email'],
                    'phone' => $this->findPhonenumbersByContactId($row['id']),
                    'contact_description' => $row['contact_description'],
                    'id_user' => $this->findOneUserById($row['id_user']),
                    'date_created' => $row['date_created'],
                );
            }
            // Free result set
            $result->close();
        } else
            echo($this->db->error);

        return $posts;
    }


    /**
     * Get contact from the database
     * @return array
     */
    public function findContact($id)
    {
        $contact = array();
        $query = ""
            . "SELECT contact.*"
            . "FROM contact "
            . "WHERE contact.id =".$id ;
        $result = $this->db->query($query);
        if ($result)
        {
            // Cycle through results
            while ($row = $result->fetch_assoc()) {
                $contact[] = array(
                    'id' => $row['id'],
                    'firstname' => $row['firstname'],
                    'lastName' => $row['lastname'],
                    'birthdate' => $row['birthdate'],
                    'contact_url' => $row['contact_url'],
                    'contact_email' => $row['contact_email'],
                    'phone' => $this->findPhonenumbersByContactId($row['id']),
                    'contact_description' => $row['contact_description'],
                    'id_user' => $this->findOneUserById($row['id_user']),
                    'date_created' => $row['date_created'],
                );
            }
            // Free result set
            $result->close();
        } else
            echo($this->db->error);

        return $contact;
    }

    /**
     * Get phonenumbers by contact ID
     * @param Int $id
     * @return array
     */
    public function findPhonenumbersByContactId($id)
    {
        $query = ""
            . "SELECT id, phonenumber "
            . "FROM phonenumbers "
            . "WHERE id_contact = '%d'";
        $query = sprintf($query, $this->db->real_escape_string($id));
        if ($result = $this->db->query($query))
        {
            if ($rows = $result->fetch_all())
            {
                foreach ($rows as $row => $key)
                {
                $numbers[$key[0]] = $key[1];
                }
                $result->close();
            }
        } else
            die($this->db->error);
        return $numbers;
    }

    /**
     * Get one contact by it's ID
     * @param Int $id
     * @return array
     */
    public function findOneUserById($id)
    {
        $user = array();
        $query = ""
                . "SELECT *"
                . "FROM user "
                . "WHERE id = '%d'";
        $query = sprintf($query, $this->db->real_escape_string($id));
        if ($result = $this->db->query($query))
        {
            $row = $result->fetch_assoc();
            $user = array(
                'id' => $row['id'],
                'name' => $row['name'],
                'email' => $row['email'],
            );

            $result->close();
        } else
            die($this->db->error);
        return $user;
    }



    
    public function addContact($firstname, $lastname, $birthdate, $contact_url, $contact_email, $phone, $contact_description, $userid)
    {

        $query =  "INSERT INTO contact(`firstname`, `lastname`, `birthdate`, `contact_url`, `contact_email`, `contact_description`, `status`, id_user) "
                . "VALUES ( '%s', '%s','%s','%s','%s','%s', 2, '%d')";
        $query = sprintf($query, $this->db->real_escape_string($firstname),
                $this->db->real_escape_string($lastname),
                $this->db->real_escape_string($birthdate),
                $this->db->real_escape_string($contact_url),
                $this->db->real_escape_string($contact_email),
                $this->db->real_escape_string($contact_description),
                $this->db->real_escape_string($userid) );
        if ($result = $this->db->query($query))
        {
             $contactId = $this->db->insert_id;
        } else
        {
            die($this->db->error);
        }
        foreach ($phone as $id => $phonenumber) {
            $phonenum = "INSERT INTO phonenumbers(`phonenumber`, `id_contact`,`status`) "
                . "VALUES ( '%d', '%d', 2)";
            $phonenum = sprintf($phonenum, $this->db->real_escape_string($phonenumber),
                $this->db->real_escape_string($contactId));
            if ($result3 = $this->db->query($phonenum)) {
            } else
            {
                die($this->db->error);
            }
        }
    }

    public function editContact($contactId ,$firstname, $lastname, $birthdate, $contact_url, $contact_email,
                                $phone, $contact_description)
    {
        $query = "UPDATE contact
                SET `firstname` = '$firstname', `lastname` = '$lastname', `birthdate` = '$birthdate', 
                `contact_url` = '$contact_url', `contact_email` = '$contact_email', 
                `contact_description` = '$contact_description' WHERE id = '$contactId' ";

        foreach ($phone as $id => $phonenumber)
        {
            if(array_key_exists('new', $phone))
            {
                foreach($phone['new'] as $arrKey => $newphonenumber)
                {
                    $query2 = "INSERT INTO phonenumbers(`phonenumber`, `id_contact`,`status`) "
                        . "VALUES ( '%d', '%d', 2)";
                    $query2 = sprintf($query2,$this->db->real_escape_string($newphonenumber),
                        $this->db->real_escape_string($contactId));
                    if($result2 = $this->db->query($query2))
                    {
                        return true;
                    } else
                        die($this->db->error);
                }

            }
            else
            {
                $query3 = "UPDATE phonenumbers 
                SET `phonenumber` = '$phonenumber' WHERE id = ".$id;
                if ($result3 = $this->db->query($query3))
                {
                    return true;
                } else
                    die($this->db->error);
            }
        }

        $query = sprintf(
            $query, $this->db->real_escape_string($contactId),
            $this->db->real_escape_string($firstname),
            $this->db->real_escape_string($lastname),
            $this->db->real_escape_string($birthdate),
            $this->db->real_escape_string($contact_url),
            $this->db->real_escape_string($contact_email),
            $this->db->real_escape_string($contact_description));
        if ($result = $this->db->query($query))
        {
            return true;
        } else
            die($this->db->error);
    }
    


}
