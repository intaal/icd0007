<?php

class IndexController
{

    private $appManager;

    private $userManager;

    public function __construct($appModel, $userModel)
    {
        $this->appManager = $appModel;
        $this->userManager = $userModel;
    }


    public function aboutCmd()
    {
        $View = new appView($this->appManager);
        $View->renderView();
    }

    public function loginCmd()
    {
        if ($this->userManager->is_logined())
        {
            $this->redirectCmd();
        }
        
        $View = new appView($this->appManager);
        $View->renderView();
    }

    public function loginsubmittedCmd($request)
    {
        $res = $this->userManager->login($request['username'], $request['password']);
        if ($res) $this->redirectCmd();
        else $this->redirectCmd("/?cmd=login&error=error");
    }

    public function logoutCmd()
    {
        $this->userManager->logout();
        $this->redirectCmd();
    }
    public function langCmd($lang)
    {
        if($lang['set'] == "ee" OR $lang['set'] == "en") {
            setcookie("lang", $lang['set'], time() + 60 * 60 * 24 * 30);
            $this->redirectCmd();
        }
        else
        {
            $this->redirectCmd();
        }
    }
    
    public function redirectCmd($route="/")
    {
        header("location: $route");
        exit;
    }

}
