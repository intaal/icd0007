<?php
class appcontroller
{
    /*
     * $appmanager model instance
     */
    private $appmanager;
    private $usermanager;

    private function cleanRequest($request)
    {
        // Walk through the array and escape all apostophes (anonymous function)
        array_walk_recursive($request, function(&$value, &$key) {
            $value = str_replace("'", "", $value);
            $key = str_replace("'", "", $key);
        });
        return $request;
    }

    public function __construct($appmodel, $usermodel)
    {
        $this->appmanager = $appmodel;
        $this->usermanager = $usermodel;
    }

    public function indexcmd($request)
    {
        if($this->usermanager->is_logined())
        {
            $posts = $this->appmanager->findallcontacts();
            $view = new appview($this->appmanager);
            $view->renderview($posts);
        }
        else $this->redirectcmd("/?cmd=login");

    }

    public function editcmd($request)
    {
        if($this->usermanager->is_logined())
        {
        $request = $this->cleanRequest($request);
        $posts = $this->appmanager->findcontact($request['id']);
        $view = new appview($this->appmanager);
        $view->renderview($posts);
        }
        else $this->redirectcmd("/?cmd=login");
    }


    public function addcmd($request)
    { if($this->usermanager->is_logined())
        {
        $view = new appview($this->appmanager);
        $view->renderview($request);
        }
    else $this->redirectcmd("/?cmd=login");
    }


    public function addcontactsubmittedcmd($request)
    {

        $request = $this->cleanRequest($request);
        $res = null;
        $userid = $this->usermanager->is_logined();
        if ($userid) $res = $this->appmanager->addcontact($request['firstName'], $request['lastName'], $request['birthdate'],
            $request['contact_url'], $request['contact_email'], $request['phone'], $request['contact_description'], $userid);
        if ($res) $this->redirectcmd();
        else $this->redirectcmd("/?cmd=add&error=error");
    }

    public function editcontactsubmittedcmd($request)
    {

        $request = $this->cleanRequest($request);
        $res = null;
        $userid = $this->usermanager->is_logined();
        if ($userid) $res = $this->appmanager->editcontact($request['id'], $request['firstName'], $request['lastName'], $request['birthdate'],
            $request['contact_url'], $request['contact_email'], $request['phone'], $request['contact_description'], $userid);
        if ($res) $this->redirectcmd();
        else $this->redirectcmd("/?cmd=edit&error=error");
    }

    public function redirectcmd($route="/")
    {
        header("location: $route");
        exit;
    }
}